import { suite, test } from "mocha"
import { assert } from "chai"

import { GreetingService, validateName } from "./index"

suite( "greeting service", function () {

    test( "build a default greeting", function () {
        const sut = new GreetingService()

        const actual = sut.build()

        assert.equal( actual.text, "Hello, World!" )
    } )

    test( "build greeting with name", function () {
        const sut = new GreetingService()

        const actual = sut.build( "Joe" )

        assert.equal( actual.text, "Hello, Joe!" )
    } )
} )

suite( "greeting form", function () {

    test( "name must be greater than 2 characters", function () {
        assert.isTrue( validateName( "joe" ) )
        assert.isFalse( validateName( "je" ) )
    } )
} )
