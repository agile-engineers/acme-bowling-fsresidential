export class GreetingService {
    build ( name: string = "World" ) {
        return { text: `Hello, ${ name }!` }
    }
}

export function validateName(name: string) {
    return name.length > 2
}

