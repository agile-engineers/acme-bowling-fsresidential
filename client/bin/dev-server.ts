import * as Path from "path"
import * as Express from "express"
import * as Browserify from "browserify"

const app = Express()

app.get( [ "/vue.min.js", "/vue.js" ], function ( req, res ) {
    res.sendFile( Path.resolve( Path.join( "vendor", req.originalUrl ) ) )
} )

app.get( "**/*.js", function ( req, res ) {

    const bundle_name = Path.basename( req.url, ".js" ).toLowerCase()
    const browserify = Browserify( { debug: true, standalone: bundle_name } )

    const index_script = Path.join( "src", req.url )

    console.log( `bundling index script: ${ index_script } as ${ bundle_name }` )

    browserify.external( [] )
    browserify.add( index_script )

    browserify.bundle( function ( error, buffer ) {
        if ( error ) {
            res.status( 500 ).send( "" )
        }
        else {
            res.set( "content-type", "application/javascript" )
            res.send( buffer )
        }
    } )
} )

app.use( Express.static( "src" ) )

app.listen( 8080, function () {
    console.log( "server started" )
} )

