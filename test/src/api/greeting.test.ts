import { assert } from "chai"
import { suite, test } from "mocha"

import { Assertions, Database, URLClient } from "./utils"

suite( "greeting", function () {
    const url = URLClient.withBaseURL( "http://localhost:8081" )

    test( "fetch a default greeting", function () {
        return url.get( "/greeting" )
            .then( Assertions.responseEquals( { text: "Hello, World!" } ) )
    } )

    test( "fetch greeting with name", function () {
        return url.get( "/greeting/joe" )
            .then( Assertions.responseEquals( { text: "Hello, joe!" } ) )
    } )

    test( "fetch greeting with name and language", function () {
        return url.get( "/greeting/joe?language=spanish" )
            .then( Assertions.responseEquals( { text: "Hola, joe!" } ) )
    } )
} )