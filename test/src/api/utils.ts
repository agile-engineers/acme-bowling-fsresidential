import * as NativeHTTP from "http"
import * as URL from "url"
import * as Knex from "knex"

import { assert } from "chai"

export class URLClient {
    protocol: string
    hostname: string
    port: string

    private constructor ( baseUrl: string ) {
        const baseUrlParts = URL.parse( baseUrl )
        this.protocol = baseUrlParts.protocol
        this.hostname = baseUrlParts.hostname
        this.port = baseUrlParts.port
    }

    static withBaseURL ( url: string ): URLClient {
        return new URLClient( url )
    }

    private static createRequestHeaders ( token?: string ) {
        const headers = {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }

        if ( token ) {
            headers[ "Authorization" ] = `Bearer ${ token }`
        }

        return headers
    }

    get ( path: string, token?: string ): Promise<any> {
        const options = {
            protocol: this.protocol,
            hostname: this.hostname,
            port: this.port,
            path: path,
            method: "GET",
            headers: URLClient.createRequestHeaders( token )
        }

        return new Promise( ( resolve, reject ) => {
            const req = NativeHTTP.request( options, function ( res ) {
                let response_data: string[] = []

                res.setEncoding( "utf8" )

                res.on( "data", function ( chunk: string ) {
                    response_data.push( chunk )
                } )

                res.on( "end", function () {
                    let body = undefined

                    try {
                        body = JSON.parse( response_data.join( "" ) )
                    } catch ( e ) {
                        // TODO: need to reject here or something
                    }

                    resolve( {
                        status: res.statusCode,
                        headers: res.headers,
                        body: body
                    } )
                } )
            } )

            req.on( "error", function ( err ) {
                reject( err )
            } )

            req.end()
        } )
    }

    post ( path: string, data: any, token?: string ): Promise<any> {
        const serialized_data = JSON.stringify( data )
        const request_headers = URLClient.createRequestHeaders( token )

        request_headers[ "Content-Length" ] = serialized_data.length

        const options = {
            protocol: this.protocol,
            hostname: this.hostname,
            port: this.port,
            path: path,
            method: "POST",
            headers: request_headers
        }

        return new Promise( ( resolve, reject ) => {
            const req = NativeHTTP.request( options, function ( res ) {
                let response_data: string[] = []

                res.setEncoding( "utf8" )

                res.on( "data", function ( chunk: string ) {
                    response_data.push( chunk )
                } )

                res.on( "end", function () {
                    let body = undefined

                    try {
                        body = JSON.parse( response_data.join( "" ) )
                    } catch ( e ) {
                        // TODO: need to reject here or something
                    }

                    resolve( {
                        status: res.statusCode,
                        headers: res.headers,
                        body: body
                    } )
                } )
            } )

            req.on( "error", function ( error ) {
                reject( error )
            } )

            req.write( serialized_data )
            req.end()
        } )
    }
}

export class Assertions {
    static responseMatches ( expected ) {
        return function ( response ) {
            assert.equal( response.status, 200 )

            const actual = response.body

            Object.keys( expected ).forEach( function ( key ) {
                assert.match( actual[ key ], expected[ key ] )
            } )

        }
    }

    static responseEquals ( expected ) {
        return function ( response ) {
            assert.equal( response.status, 200 )
            assert.deepEqual( response.body, expected )
        }
    }

    static failsWith ( expected_status: number ) {
        return function ( response ) {
            assert.equal( response.status, expected_status )
        }
    }
}

export class Database {
    constructor ( private connection ) {
    }

    static connect () {
        const knex = Knex( {
            client: "postgresql",
            connection: {
                database: "postgres",
                user: "postgres",
                password: "password"
            }
        } )

        return new Database( knex )
    }

    clearSessions = () =>
        this.connection( "sessions" ).where( { account_id: "2" } ).delete()

    close = () =>
        this.connection.destroy()
}

