import { assert } from "chai"
import { suite, test } from "mocha"

import { assertTextAs, initializeWebDriver, takeScreenShot } from "./utils"

suite( "home page", function () {

    beforeEach( function () {
        this.driver = initializeWebDriver()

        return this.driver
            .url( "http://localhost:8080/" )
    } )

    afterEach( function () {
        return this.driver.end()
    } )

    test( "a default greeting is displayed", function () {
        return this.driver.then( assertTextAs( "p", "Hello, World!" ) )
    } )

    test( "a custom greeting given a name", function () {
        return this.driver
            .setValue( "form input", "joe" )
            .click( "form button" )
            .then( takeScreenShot )
            .then( assertTextAs( "p", "Hello, joe!" ) )
    } )
} )