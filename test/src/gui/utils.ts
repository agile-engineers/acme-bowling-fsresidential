import { assert } from "chai"
import { remote } from "webdriverio"

export const initializeWebDriver = function () {
    return remote( { desiredCapabilities: { browserName: "chrome" } } ).init()
}

export const assertTextAs = ( selector, expected ) => function () {
    return this.getText( selector ).then( actual => assert.equal( actual, expected ) )
}

export const assertListAs = ( selector, expected ) => function () {
    return this.getText( selector ).then( actual => assert.deepEqual( actual, expected ) )
}

export const assertCountAs = ( selector, expected ) => function () {
    return this.elements( selector ).then( actual => {
        assert.equal( actual.value.length, expected )
    } )
}

export const cleanupWebDriver = function () {
    return this.driver.end()
}

export const takeScreenShot = function () {
    return this.saveScreenshot( `${ process.cwd() }/screen-${ Date.now() }.png` )
}

export const users = {
    "joe": { email: "joe@m.com", password: "pass" }
}
