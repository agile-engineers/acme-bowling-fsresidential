using ApiWeb.Greetings;
using Moq;
using Xunit;

namespace ApiTest.Greetings
{
    public class GreetingServiceTests
    {
        private readonly GreetingService _sut;
        private readonly Mock<ITranslationGateway> _translationGateway;

        public GreetingServiceTests()
        {
            _translationGateway = new Mock<ITranslationGateway>();
            _sut = new GreetingService(_translationGateway.Object);
        }

        [Fact]
        public void build_a_default_greeting()
        {
            Greeting actual = _sut.Build();

            Assert.Equal("Hello, World!", actual.Text);
        }

        [Fact]
        public void build_greeting_with_name()
        {
            Greeting actual = _sut.Build("Joe");

            Assert.Equal("Hello, Joe!", actual.Text);
        }

        [Fact]
        public void build_greeting_for_specific_language()
        {
            _translationGateway
                .Setup(gateway => gateway.Translate("spanish"))
                .Returns("Hola");
            
            Greeting actual = _sut.Build("Joe", "spanish");

            Assert.Equal("Hola, Joe!", actual.Text);
        }
    }
}