﻿using Microsoft.AspNetCore.Mvc;

namespace ApiWeb.Greetings
{
    [Route("greeting")]
    public class GreetingController : Controller
    {
        private readonly GreetingService _service;

        public GreetingController(ITranslationGateway translationGateway)
        {
            _service = new GreetingService(translationGateway);
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_service.Build());
        }

        [HttpGet("{name}")]
        public IActionResult GetByName(string name, [FromQuery] string language)
        {
            if (language == null)
            {
                return Ok(_service.Build(name));
            }

            return Ok(_service.Build(name, language));
        }
    }
}