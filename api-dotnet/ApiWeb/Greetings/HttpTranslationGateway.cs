using System;
using System.Net.Http;
using Newtonsoft.Json;

namespace ApiWeb.Greetings
{
    public class TranslationResponse
    {
        public string Text { get; set; }
    }

    public class HttpTranslationGateway : ITranslationGateway
    {
        public string Translate(string language)
        {
            var url =
                $"https://grn4aewhhg.execute-api.us-east-1.amazonaws.com/prod/salutations/{language}";

            var client = new HttpClient();
            var responseText = client.GetStringAsync(url).Result;

            var response = JsonConvert.DeserializeObject<TranslationResponse>(responseText);

            return response.Text;
        }
    }
}