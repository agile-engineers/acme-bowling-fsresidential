namespace ApiWeb.Greetings
{
    public interface ITranslationGateway
    {
        string Translate(string language);
    }
}