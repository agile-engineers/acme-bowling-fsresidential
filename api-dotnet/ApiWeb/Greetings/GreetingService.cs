﻿namespace ApiWeb.Greetings
{
    public class Greeting
    {
        public string Text { get; set; }
    }

    public class GreetingService
    {
        private readonly ITranslationGateway _translationGateway;

        public GreetingService(ITranslationGateway translationGateway)
        {
            _translationGateway = translationGateway;
        }

        public Greeting Build()
        {
            return Build("World");
        }

        public Greeting Build(string name)
        {
            return new Greeting {Text = $"Hello, {name}!"};
        }

        public Greeting Build(string name, string language)
        {
            return new Greeting
            {
                Text = $"{_translationGateway.Translate(language)}, {name}!"
            };
        }
    }
}