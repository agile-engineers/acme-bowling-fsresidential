﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace ApiWeb
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        private static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls("http://localhost:8081")
                .UseStartup<Startup>()
                .Build();
    }
}
