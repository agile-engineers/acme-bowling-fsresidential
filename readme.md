# ACME Bowling

## Clone and Config git repository

Launch a console and execute in: `~\Projects` directory

```bash
git clone https://goingagile0@bitbucket.org/agile-engineers/acme-bowling-fsresidential.git

cd acme-bowling-fsresidential

git config user.email "goingagile#@gmail.com"
git config user.name "your-name"

git checkout -b your-name
```

## Execute API

Launch a console and execute in: `Projects\acme-bowling-fsresidential\api-dotnet\ApiWeb` directory

```bash
dotnet run
```

## Execute Client

Launch a console and execute in: `Projects\acme-bowling-fsresidential\client` directory

```bash
npm start
```

## Execute Selenium

Launch a console and execute in: `Projects\acme-bowling-fsresidential\test` directory

```bash
npm install
npm run selenium-install
npm run selenium
```

## Run GUI Tests

Launch a console and execute in: `Projects\acme-bowling-fsresidential\test` directory

```bash
npm run gui-tests
```
